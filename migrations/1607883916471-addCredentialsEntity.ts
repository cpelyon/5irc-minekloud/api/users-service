import { MigrationInterface, QueryRunner } from 'typeorm';

export class addCredentialsEntity1607883916471 implements MigrationInterface {
	name = 'addCredentialsEntity1607883916471';

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`
            CREATE TABLE "credentials" (
                "id" integer NOT NULL,
                "password" character varying NOT NULL,
                "userId" integer NOT NULL,
                CONSTRAINT "REL_8d3a07b8e994962efe57ebd0f2" UNIQUE ("userId"),
                CONSTRAINT "PK_1e38bc43be6697cdda548ad27a6" PRIMARY KEY ("id")
            )
        `);
		await queryRunner.query(`
            ALTER TABLE "credentials"
            ADD CONSTRAINT "FK_8d3a07b8e994962efe57ebd0f20" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION
        `);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`
            ALTER TABLE "credentials" DROP CONSTRAINT "FK_8d3a07b8e994962efe57ebd0f20"
        `);
		await queryRunner.query(`
            DROP TABLE "credentials"
        `);
	}
}
