import { Logger, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { version } from '../package.json';
import { ConfigService } from '@nestjs/config';

async function bootstrap() {
	const logger = new Logger('Main');

	const app = await NestFactory.create(AppModule, { cors: true });
	app.setGlobalPrefix('/api');
	app.useGlobalPipes(
		new ValidationPipe({
			whitelist: true,
			transform: true,
		}),
	);

	const options = new DocumentBuilder()
		.setTitle('Users Service')
		.setDescription('User and Auth service')
		.setVersion(version)
		.addBearerAuth()
		.build();

	const document = SwaggerModule.createDocument(app, options);
	SwaggerModule.setup('api/docs/users', app, document);


	const configService = await app.resolve(ConfigService);

	const env = configService.get('app.environment');
	logger.log(`Current environement is ${env}`);

	const port = configService.get<number>('app.port');
	logger.log(`Start listening on port ${port}`);

	await app.listen(port);
}
bootstrap();
