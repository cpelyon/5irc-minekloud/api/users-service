import { Environment } from './environement.enum';

export const configuration = () => {
	if (!process.env.AUTH_JWT_SECRET) {
		throw new Error(
			'AUTH_JWT_SECRET must be set. Hint: if your in dev environement, check out the .env.template file',
		);
	}

	return {
		app: {
			port: process.env.APP_PORT || 3000,
			environment: process.env.NODE_ENV || Environment.Production,
		},
		auth: {
			jwtSecret: process.env.AUTH_JWT_SECRET,
			jwtDuration: process.env.AUTH_JWT_DURATION || '10m',
		},
		unleash: {
			url: process.env.UNLEASH_URL,
			instanceId: process.env.UNLEASH_INSTANCE_ID,
			appName: process.env.UNLEASH_APP_NAME || process.env.NODE_ENV,
		},
	};
};
