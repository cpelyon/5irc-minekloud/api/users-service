export enum FeatureFlags {
	DISABLE_REGISTRATION = 'disable_registration',
	DISABLE_LOGIN = 'disable_login',
}
