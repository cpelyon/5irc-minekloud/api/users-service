import { ModuleMetadata, Type } from '@nestjs/common';

export interface UnleashModuleOptions {
	global?: boolean;
	url: string;
	appName: string;
	instanceId: string;
}

export interface UnleashModuleAsyncOptions
	extends Pick<ModuleMetadata, 'imports'> {
	global?: boolean;
	useExisting?: Type<UnleashModuleOptionsFactory>;
	useClass?: Type<UnleashModuleOptionsFactory>;
	useFactory?: (
		...args: any[]
	) => Promise<UnleashModuleOptions> | UnleashModuleOptions;
	inject?: any[];
}

export interface UnleashModuleOptionsFactory {
	createUnleashOptions(): Promise<UnleashModuleOptions> | UnleashModuleOptions;
}
