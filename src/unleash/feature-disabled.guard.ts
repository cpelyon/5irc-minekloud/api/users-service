import {
	CanActivate,
	ExecutionContext,
	Injectable,
	mixin,
} from '@nestjs/common';
import { UnleashService } from './unleash.service';

export const FeatureDisabledGuard = (flag: string, defaultValue?: boolean) => {
	@Injectable()
	class FeatureDisabledGuardMixing implements CanActivate {

		constructor(private readonly unleashService: UnleashService) {}

		canActivate(): boolean {
			return !this.unleashService.isEnabled(flag, {}, defaultValue);
		}
	}

	const guard = mixin(FeatureDisabledGuardMixing);
	return guard;
};