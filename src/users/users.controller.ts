import {
	Controller,
	Get,
	Body,
	Param,
	Delete,
	ParseIntPipe,
	Patch,
	UseGuards,
	NotFoundException,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { UpdateUserDto } from './dto/update-user.dto';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import {
	ApiBadRequestResponse,
	ApiBearerAuth,
	ApiNotFoundResponse,
	ApiTags,
	ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { User } from './entities/user.entity';
@ApiTags('users')
@ApiBearerAuth()
@ApiUnauthorizedResponse()
@Controller('users')
@UseGuards(JwtAuthGuard)
export class UsersController {
	constructor(private readonly usersService: UsersService) {}

	@Get()
	findAll(): Promise<User[]> {
		return this.usersService.findAll();
	}

	@ApiNotFoundResponse()
	@Get(':id')
	async findOne(@Param('id', ParseIntPipe) id: number): Promise<User> {
		const user = await this.usersService.findOne(+id);
		if (!user) {
			throw new NotFoundException(`User with id ${id} not found`);
		}
		return user;
	}

	@ApiNotFoundResponse()
	@ApiBadRequestResponse()
	@Patch(':id')
	async update(
		@Param('id', ParseIntPipe) id: number,
		@Body() updateUserDto: UpdateUserDto,
	): Promise<User> {
		const user = await this.usersService.update(id, updateUserDto);
		if (!user) {
			throw new NotFoundException(`User with id ${id} not found`);
		}
		return user;
	}

	@ApiNotFoundResponse()
	@Delete(':id')
	async remove(@Param('id', ParseIntPipe) id: number): Promise<User> {
		const user = await this.usersService.remove(id);
		if (!user) {
			throw new NotFoundException(`User with id ${id} not found`);
		}
		return user;
	}
}
