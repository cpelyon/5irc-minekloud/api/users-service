import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeepPartial, Repository } from 'typeorm';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService {
	constructor(
		@InjectRepository(User)
		private usersRepository: Repository<User>,
	) {}

	create(partialUser: DeepPartial<User>): Promise<User> {
		const user = this.usersRepository.create(partialUser);
		return this.usersRepository.save(user);
	}

	async update(
		id: number,
		partialUser: DeepPartial<User>,
	): Promise<User | false> {
		const user = await this.usersRepository.preload({ id, ...partialUser });
		if (!user) {
			return false;
		}
		return this.usersRepository.save(user);
	}

	findAll(): Promise<User[]> {
		return this.usersRepository.find();
	}

	async findOne(id: number): Promise<User | false> {
		const user = this.usersRepository.findOne(id);
		if (!user) {
			return false;
		}
		return user;
	}

	async findOneByUsername(username: string): Promise<User | false> {
		const user = await this.usersRepository.findOne({ username });
		if (!user) {
			return false;
		}
		return user;
	}

	async findOneByEmail(email: string): Promise<User | false> {
		const user = await this.usersRepository.findOne({ email });
		if (!user) {
			return false;
		}
		return user;
	}

	async remove(id: number): Promise<User | false> {
		const user = await this.findOne(id);
		if (!user) {
			return false;
		}
		return this.usersRepository.remove(user);
	}
}
